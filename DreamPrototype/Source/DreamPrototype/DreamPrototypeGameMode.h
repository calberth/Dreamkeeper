// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "DreamPrototypeGameMode.generated.h"

UCLASS(minimalapi)
class ADreamPrototypeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADreamPrototypeGameMode();
};



